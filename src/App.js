import React from 'react';
import logo from './logo.svg';
import './App.css';
import { requestNotes } from './api';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const useStyles = {
  card: {

    marginTop: 100,
    minWidth: 275,
    backgroundColor: "#32a852"

  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  }
};

class App extends React.Component {

  state = {
    datas: []
  }

  componentDidMount() {
    requestNotes().then((data) => this.setState({ datas: data }))
  }

  render() {

    return (<div>{this.state.datas.map(item => (
      <Card style={useStyles.card}>
        <CardContent>
          <Typography style={useStyles.title} color="textSecondary" gutterBottom>
            {item.title}
          </Typography>
          <Typography variant="h5" component="h2">
            {item.description}
          </Typography>
        </CardContent>
      </Card>
    ))
    }
    </div>
    )
  }

}


export default App;
